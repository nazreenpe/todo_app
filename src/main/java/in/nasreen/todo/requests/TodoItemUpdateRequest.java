package in.nasreen.todo.requests;

public class TodoItemUpdateRequest {
    private String title;
    private Boolean completed;
    private Integer order;

    public TodoItemUpdateRequest() {
    }

    public TodoItemUpdateRequest(String title, boolean completed, int order) {
        this.title = title;
        this.completed = completed;
        this.order = order;
    }

    public String getTitle() {
        return title;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public Integer getOrder() {
        return order;
    }
}
