package in.nasreen.todo.requests;

import org.springframework.stereotype.Service;

@Service
public class TodoListUpdateRequest {
    private String title;

    public TodoListUpdateRequest() {
    }

    public TodoListUpdateRequest(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
