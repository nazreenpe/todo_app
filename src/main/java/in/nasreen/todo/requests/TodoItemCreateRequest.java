package in.nasreen.todo.requests;

public class TodoItemCreateRequest {
    private String title;

    public TodoItemCreateRequest() {
    }

    public TodoItemCreateRequest(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
