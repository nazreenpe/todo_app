package in.nasreen.todo.requests;

public class TodoListCreateRequest {
    private String title;

    public TodoListCreateRequest() {
    }

    public TodoListCreateRequest(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
