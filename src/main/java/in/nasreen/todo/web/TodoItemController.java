package in.nasreen.todo.web;

import in.nasreen.todo.model.TodoItem;
import in.nasreen.todo.requests.TodoItemCreateRequest;
import in.nasreen.todo.requests.TodoItemUpdateRequest;
import in.nasreen.todo.service.TodoItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/items")
public class TodoItemController {
    private TodoItemService todoItemService;

    @Autowired
    public TodoItemController(TodoItemService todoItemService) {
        this.todoItemService = todoItemService;
    }

    @RequestMapping(path = "", method = RequestMethod.DELETE)
    public void deleteAll() {
        todoItemService.deleteAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TodoItem> show(@PathVariable(name = "id") String identifier) {
        return todoItemService.getItem(identifier)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @RequestMapping(path = "/{id}",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TodoItem> update(@PathVariable(name = "id") String identifier,
                                           @RequestBody TodoItemUpdateRequest request) {
        return todoItemService.update(identifier, request)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @RequestMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteItem(@PathVariable(name = "id") String identifier) {
        todoItemService.delete(identifier);
    }
}
