package in.nasreen.todo.web;

import in.nasreen.todo.model.TodoItem;
import in.nasreen.todo.model.TodoList;
import in.nasreen.todo.requests.TodoItemCreateRequest;
import in.nasreen.todo.requests.TodoListCreateRequest;
import in.nasreen.todo.requests.TodoListUpdateRequest;
import in.nasreen.todo.service.TodoItemDatabaseService;
import in.nasreen.todo.service.TodoListItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/lists")
public class TodoListController {
   private TodoListItemService todoListItemService;
   private TodoItemDatabaseService todoItemDatabaseService;

   @Autowired
    public TodoListController(TodoListItemService todoListItemService, TodoItemDatabaseService todoItemDatabaseService) {
        this.todoListItemService = todoListItemService;
        this.todoItemDatabaseService = todoItemDatabaseService;
    }

    @RequestMapping(path = "/{id}/items",
    method = RequestMethod.POST,
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TodoItem> createItem(@PathVariable(name = "id") String identifier,
                                               @RequestBody TodoItemCreateRequest todoItemCreateRequest) {
        return todoListItemService.getList(identifier)
                .map(todoList -> {
                    TodoItem todoItem = new TodoItem(todoItemCreateRequest.getTitle());
                    todoItem.setList(todoList);
                    todoItemDatabaseService.add(todoItem);
                    return ResponseEntity.ok(todoItem);
                })
                .orElse(ResponseEntity.notFound().build());
    }

    @RequestMapping(path = "/{id}/items", method = RequestMethod.GET)
    public ResponseEntity getItems(@PathVariable(name = "id") String listId) {
       return todoListItemService.getList(listId)
               .map(todoList -> ResponseEntity.ok(todoItemDatabaseService.findByListId(listId)))
               .orElse(ResponseEntity.notFound().build());
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public List list() {
       return todoListItemService.getAll();
    }

    @RequestMapping(path = "", method = RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public TodoList create(@RequestBody TodoListCreateRequest todoListCreateRequest) {
        return todoListItemService.add(todoListCreateRequest.getTitle());
    }

    @RequestMapping(path = "", method = RequestMethod.DELETE)
    public void deleteAll() {
       todoListItemService.deleteAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PATCH,
                    produces = MediaType.APPLICATION_JSON_VALUE,
                    consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TodoList> update(@PathVariable (name = "id") String identifier,
                                 @RequestBody TodoListUpdateRequest todoListUpdateRequest) {
       return todoListItemService.update(identifier, todoListUpdateRequest.getTitle())
               .map(ResponseEntity::ok)
               .orElse(ResponseEntity.notFound().build());
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void deleteList(@PathVariable (name = "id") String identifier) {
       todoListItemService.deleteList(identifier);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<TodoList> showList(@PathVariable(name = "id") String identifier) {
       return todoListItemService.getList(identifier)
               .map(ResponseEntity::ok)
               .orElse(ResponseEntity.notFound().build());
    }

}
