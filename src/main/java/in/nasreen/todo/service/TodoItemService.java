package in.nasreen.todo.service;

import in.nasreen.todo.model.TodoItem;
import in.nasreen.todo.requests.TodoItemUpdateRequest;

import java.util.List;
import java.util.Optional;

public interface TodoItemService {
    TodoItem add(TodoItem todoItem);
    void delete(String id);
    Optional<TodoItem> update(String identifier, TodoItemUpdateRequest request);
    void deleteAll();
    Optional<TodoItem> getItem(String id);
    List<TodoItem> getAll();
}
