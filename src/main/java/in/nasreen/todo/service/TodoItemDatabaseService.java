package in.nasreen.todo.service;

import in.nasreen.todo.db.TodoItemRepository;
import in.nasreen.todo.model.TodoItem;
import in.nasreen.todo.requests.TodoItemUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TodoItemDatabaseService implements TodoItemService {
    private TodoItemRepository todoItemRepository;

    @Autowired
    public TodoItemDatabaseService(TodoItemRepository todoItemRepository) {
        this.todoItemRepository = todoItemRepository;
    }

    @Override
    public TodoItem add(TodoItem todoItem) {
        return todoItemRepository.save(todoItem);
    }

    @Override
    public void delete(String id) {
        todoItemRepository.deleteById(id);
    }

    @Override
    public Optional<TodoItem> update(String identifier, TodoItemUpdateRequest request) {
        return todoItemRepository.findById(identifier)
                .map(todoItem -> {
                    if (request.getTitle() != null) {
                        todoItem.setTitle(request.getTitle());
                    }
                    if (request.getCompleted()!= null) {
                        todoItem.setCompleted(request.getCompleted());
                    }
                    if (request.getOrder() != null) {
                        todoItem.setOrder(request.getOrder());
                    }
                    todoItemRepository.save(todoItem);
                    return todoItem;
                });
    }

    @Override
    public void deleteAll() {
        todoItemRepository.deleteAll();
    }

    @Override
    public Optional<TodoItem> getItem(String id) {
        return todoItemRepository.findById(id);
    }

    @Override
    public List<TodoItem> getAll() {
        return todoItemRepository.findAll();
    }

    public List<TodoItem> findByListId(String id) {
        return todoItemRepository.findByListId(id);
    }
}
