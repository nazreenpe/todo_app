package in.nasreen.todo.service;

import in.nasreen.todo.model.TodoItem;
import in.nasreen.todo.requests.TodoItemUpdateRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class InMemoryItemService implements TodoItemService {
    private List<TodoItem> todoItems;

    public InMemoryItemService() {
        todoItems = new ArrayList<>();
    }

    @Override
    public TodoItem add(TodoItem todoItem) {
        todoItems.add(todoItem);
        return todoItem;
    }

    @Override
    public void delete(String id) {
        todoItems.stream()
                .filter(todoItem -> todoItem.getId().equals(id))
                .findFirst()
                .map(todoItem -> todoItems.remove(todoItem));
    }

    @Override
    public Optional<TodoItem> update(String id, TodoItemUpdateRequest request) {
        return todoItems.stream()
                .filter(todoItem -> todoItem.getId().equals(id))
                .findFirst()
                .map(todoItem -> {
                    todoItem.setCompleted(request.getCompleted());
                    todoItem.setTitle(request.getTitle());
                    todoItem.setOrder(request.getOrder());
                    return todoItem;
                });
    }

    @Override
    public void deleteAll() {
        todoItems.clear();
    }

    @Override
    public Optional<TodoItem> getItem(String id) {
        return todoItems.stream()
                .filter(item -> item.getId().equals(id))
                .findFirst();
    }

    @Override
    public List<TodoItem> getAll() {
        return todoItems;
    }
}
