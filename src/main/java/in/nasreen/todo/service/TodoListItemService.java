package in.nasreen.todo.service;

import in.nasreen.todo.db.TodoListRepository;
import in.nasreen.todo.model.TodoList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TodoListItemService {
    private TodoListRepository todoListRepository;

    @Autowired
    public TodoListItemService(TodoListRepository todoListRepository) {
        this.todoListRepository = todoListRepository;
    }

    public List getAll() {
        return todoListRepository.findAll() ;
    }

    public TodoList add(String title) {
        TodoList todoList = new TodoList(title);
        return todoListRepository.save(todoList);
    }

    public Optional<TodoList> update(String identifier, String title) {
        return todoListRepository.findById(identifier)
                .map(todoList -> {
                    todoList.setTitle(title);
                    todoListRepository.save(todoList);
                    return todoList;
                });
    }

    public void deleteAll() {
        todoListRepository.deleteAll();
    }

    public void deleteList(String identifier) {
        todoListRepository.deleteById(identifier);
    }

    public Optional<TodoList> getList(String identifier) {
        return todoListRepository.findById(identifier);
    }
}
