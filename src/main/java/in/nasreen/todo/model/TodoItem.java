package in.nasreen.todo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "todo_items")
@JsonIgnoreProperties(value = "listId", allowGetters = true)
public class TodoItem {
    private String title;
    private Boolean completed;
    @Id  //specifying that the field id is the primary key
    private String id;
    @Column(name ="item_order")
    private Integer order;
    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "list_id", nullable = false)
    @JsonIgnore
    private TodoList list;

    public TodoItem() {
    }

    public TodoItem(String title) {
        this.title = title;
        this.completed = false;
        id = UUID.randomUUID().toString();
        this.order = -1;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public String getId() {
        return id;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setList(TodoList todoList) {
        this.list = todoList;
    }

    public String listId() {
        if (this.list != null) {
            return list.getId();
        } else {
            return null;
        }
    }
}
