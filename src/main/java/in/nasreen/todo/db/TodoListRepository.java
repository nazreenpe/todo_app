package in.nasreen.todo.db;

import in.nasreen.todo.model.TodoList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoListRepository extends CrudRepository<TodoList, String> {
    List<TodoList> findAll();
}
