package in.nasreen.todo.db;

import in.nasreen.todo.model.TodoItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoItemRepository extends CrudRepository<TodoItem, String> {
    List<TodoItem> findAll();

    List<TodoItem> findByListId(String listId);
}
