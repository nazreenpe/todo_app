--liquibase formatted sql

--changeset nazreenpe:2

CREATE TABLE todo_lists (
    id VARCHAR(36) NOT NULL,
    title varchar not null,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);

--rollback DROP TABLE todo_lists