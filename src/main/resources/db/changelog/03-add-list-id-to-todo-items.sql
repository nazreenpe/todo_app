--liquibase formatted sql

--changeset nazreenpe:3

alter table todo_items
    add list_id VARCHAR(36) Not Null;

--rollback alter table todo_items drop column list_id;