--liquibase formatted sql

--changeset nazreenpe:1
CREATE TABLE todo_items (
   id VARCHAR(36) NOT NULL,
  title VARCHAR(256) NOT NULL,
  item_order INT,
  completed boolean,
  created_at timestamp with time zone,
  updated_at timestamp with time zone
);

--rollback DROP TABLE todo_items