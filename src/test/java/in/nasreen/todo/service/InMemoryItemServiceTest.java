package in.nasreen.todo.service;

import in.nasreen.todo.model.TodoItem;
import in.nasreen.todo.requests.TodoItemUpdateRequest;
import org.junit.Test;

import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class InMemoryItemServiceTest {

    @Test
    public void noItemsAtStarting() {
        InMemoryItemService service = new InMemoryItemService();
        assertEquals(0, service.getAll().size());
    }

    @Test
    public void canAddAnItemAndRetrieveIt() {
        InMemoryItemService service = new InMemoryItemService();
        TodoItem item = new TodoItem("Write Tests");
        service.add(item);
        assertEquals(Optional.of(item), service.getItem(item.getId()));
    }

    @Test
    public void canDeleteAnItemById() {
        InMemoryItemService service = new InMemoryItemService();
        TodoItem item = new TodoItem("Write Tests");
        service.add(item);
        service.delete(item.getId());
        assertEquals(Collections.emptyList(), service.getAll());
    }

    @Test
    public void canClearAllItems() {
        InMemoryItemService service = new InMemoryItemService();
        TodoItem item = new TodoItem("Write Tests");
        TodoItem secondItem = new TodoItem("Write Multiple Tests");
        service.add(item);
        service.add(secondItem);
        service.deleteAll();
        assertEquals(0, service.getAll().size());
    }

    @Test
    public void canUpdateAnItem() {
        InMemoryItemService service = new InMemoryItemService();
        TodoItem item = new TodoItem("Write Tests");
        service.add(item);
        TodoItem updatedItem = service.update(item.getId(),
                new TodoItemUpdateRequest("Learn Spring Injection", true, 1)).get();
        assertEquals("Learn Spring Injection", updatedItem.getTitle());
        assertEquals(true, updatedItem.getCompleted());
        assertEquals(Optional.of(1), updatedItem.getOrder());
    }
}